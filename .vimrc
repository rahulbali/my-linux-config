set nocompatible              " be iMproved, required
filetype off                  " required

set backspace=indent,eol,start

"" airline fix
let g:airline_powerline_fonts = 1

"" Terrafrm
let g:terraform_fmt_on_save=1
let g:terraform_align=1
let g:terraform_fold_sections=1

"" CPP c++
let g:cpp_no_funtion_highlight = 1
let g:cpp_attributes_highlight = 1
let g:cpp_member_highlight = 1
let g:cpp_simple_highlight = 1

"" GO vim-go
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'


call plug#begin('~/.vim/plugbali')

Plug 'ajmwagar/vim-deus'

Plug 'hashivim/vim-terraform'

Plug 'andrewstuart/vim-kubernetes'

Plug 'bfrg/vim-cpp-modern'

Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

Plug 'valloric/youcompleteme'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

call plug#end()

filetype plugin indent on
syntax on
set number



"" colo peachpuff
colo deus 
