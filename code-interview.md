
# tips for coding interview

understand question

think out loud your mindset for problems


start with describing the question
speak out a verbal solution
outline the algorithm you may use
discuss with your interviewer


then code

**make it collaborative with your interviewer**


> DS
- Arrays
- Linked List
- Stacks
- Queues
- Sets
- Maps
- Binary Trees
- Heaps
- Graphs


> Algo
- Sorting
- Searching
- Binary Search
- Divide and conquer
- Dynamic Programming / memorization
- Greedy algorithm
- Recursion
- Graph traversal, BFS, DFS


Testing / Error Checking
Malformed inputs, and how to handle them


Best Practices





